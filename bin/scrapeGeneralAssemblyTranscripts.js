const fetchHouseTranscripts = require('../src/fetchHouseTranscripts')
const getGeneralAssemblies = require('../src/getGeneralAssemblies');

const Yargs = require('yargs/yargs');
const argv = Yargs(process.argv.slice(2))
    .command('scrapeGeneralAssemblyTranscripts', 'Scrapes ilga.gov for house transcripts', (yargs) => {})
    .option('generalAssembly', {
        alias: 'a',
        type: 'int',
        description: 'If provided, scrapes only the house transcripts for the provided general assembly.'
    })
    .parse()

function wait(ms) {
    return new Promise((resolve, reject) =>  {
        setTimeout(() => {
            resolve()
        }, ms)
    })
}

const main = async () => {

    let assemblies;

    if (typeof argv.generalAssembly === 'number') {
        assemblies = [ argv.generalAssembly ];
    } else {
        assemblies = getGeneralAssemblies();
    }

    for(let i = 0; i < assemblies.length; i++) {
        try {
            const content = await fetchHouseTranscripts(assemblies[i]);
            await wait(1000);

            content.forEach(entry => {
                console.log(entry);
            })
        } catch (e) {
            console.error(`Assembly ${assemblies[i]}: Error - ${e.message}`);
        }
    }
}

main();

