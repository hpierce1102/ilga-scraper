const fetchHouseTranscripts = require('./fetchHouseTranscripts')
const getGeneralAssemblies = require('./getGeneralAssemblies');
const fs = require('fs').promises

function wait(ms) {
    return new Promise((resolve, reject) =>  {
        setTimeout(() => {
            resolve()
        }, ms)
    })
}

const main = async () => {
    // const promises = getGeneralAssemblies().map((assembly) => {
    //     return fetchHouseTranscripts(assembly);
    // });

    const assemblies = getGeneralAssemblies();

    const transcripts = [];

    for(let i = 0; i < assemblies.length; i++) {
        try {
            const content = await fetchHouseTranscripts(assemblies[i]);
            await wait(1000);

            content.forEach(entry => {
                transcripts.push(entry);
            })

            console.log(`Assembly ${assemblies[i]}: ${content.length} transcripts`)
        } catch (e) {
            console.error(`Assembly ${assemblies[i]}: Error - ${e.message}`);
        }
    }

    const file = await fs.writeFile(__dirname + '/../cache/houseTranscripts.json', JSON.stringify(transcripts, null, 2));
    // const content = await fetchHouseTranscripts(91);
    // console.log(`Assembly 91: ${content.length} transcripts`)




    //
    // Promise.all(promises).then((values) => {
    //     console.log(values);
    // })

    // const transcripts = await fetchHouseTranscripts(101);
    // console.log(transcripts);
    // const transcripts2 = await fetchHouseTranscripts(102);
    // console.log(transcripts2);
    // return;
}

main();

