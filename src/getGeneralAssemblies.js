function getGeneralAssemblies() {
    const illinoisFounded = 1818;
    const currentYear = new Date().getFullYear();

    const numberOfAssemblies = Math.ceil((currentYear - illinoisFounded) / 2);

    return [...Array(numberOfAssemblies).keys()].map((entry => {
        return entry + 1;
    }))
}

module.exports = getGeneralAssemblies;