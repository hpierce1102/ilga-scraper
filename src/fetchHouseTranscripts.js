const sendRequest = require('./sendRequest');

function parseForTranscripts(body, generalAssembly) {
    const regex = /<option value="(.*?)">(.*?)<\/option>/g

    const transcripts = [];
    let match;
    do {
        match = regex.exec(body);
        if (match && match[1].length > 0) {
            transcripts.push({
                url: `https://www.ilga.gov${match[1]}`,
                title: match[2],
                generalAssembly
            })
        }
    } while (match)

    return transcripts.filter((transcript) => transcript.url.length > 0);
}

function trimChars(string, chars) {
    return chars.reduce((carry, item) => {
        return trim(carry, item);
    }, string)
}

function trim (s, c) {
    if (c === "]") c = "\\]";
    if (c === "^") c = "\\^";
    if (c === "\\") c = "\\\\";
    return s.replace(new RegExp(
        "^[" + c + "]+|[" + c + "]+$", "g"
    ), "");
}

function getOldTranscriptFiles(body, generalAssembly) {

    let regex;
    if (generalAssembly === 91) {
        regex = /var locationArray = \[((.|\r|\n)*?)\]/g
    } else {
        regex = /var locationArrayPDF = \[((.|\r|\n)*?)\]/g
    }

    const match = regex.exec(body);

    if (match === null) {
        throw new Error('Failed to identify files');
    }

    let pieces = match[1].split('\r\n');
    pieces = pieces.map(piece => piece.trim())
    pieces = pieces.map(piece => trimChars(piece, ['"', ',', '"']))
    pieces = pieces.filter(piece => piece.length > 0);
    pieces = pieces.map(piece => `https://www.ilga.gov/house/transcripts/htrans${generalAssembly}/${piece}`)

    return pieces;
}

function parseOldTranscripts(body, generalAssembly) {
    const files = getOldTranscriptFiles(body, generalAssembly);

    const regex = /<OPTION>(.*)?[\r\n]/g

    const transcripts = [];
    let match;
    do {
        match = regex.exec(body);
        if (match) {
            transcripts.push({
                url: files.shift(),
                title: match[1],
                generalAssembly
            })
        }
    } while (match)

    return transcripts
}

async function fetchHouseTranscripts(generalAssembly) {
    if (generalAssembly < 77) {
        throw new Error('Transcript prior to 1972 are not available.');
    }

    try {
        if (generalAssembly < 93) {
            const data = await sendRequest({
                'method': 'GET',
                'hostname': 'www.ilga.gov',
                'path': `/house/transcripts/htrans${generalAssembly}/hts.html`,
            });

            return parseOldTranscripts(data.body, generalAssembly);
        } else {
            const data = await sendRequest({
                'method': 'GET',
                'hostname': 'www.ilga.gov',
                'path': `/house/transcripts/default.asp?GA=${generalAssembly}`,
            });

            return parseForTranscripts(data.body, generalAssembly);
        }
    } catch (e) {
        throw new Error(e.message);
    }
}

module.exports = fetchHouseTranscripts;